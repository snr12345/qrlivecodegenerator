//https://github.com/soldair/node-qrcode

// After generating qr using below code, to Capture the qr codes, use below apps

// Any copy -quickly copies the data
// Save Qr - scans the data

// https://play.google.com/store/apps/details?id=any.copy.io.basic

// https://play.google.com/store/apps/details?id=com.hsrk.qrsave2


var QRCode = require('qrcode')
function Qcode(){
    this.QR = QRCode;

    this.read=function(canvas,text,callback){
      
      QRCode.toCanvas(canvas, text, { width: '700' }, function (error) {
        if (error) callback('error',error)
        else
        callback('success',null)
      })
    }
    
    
}

var qcode = new Qcode();
window.qcodex=qcode;

if(typeof(module)!="undefined")
module.exports = qcode;
//var canvas = document.getElementById('canvas')
